<?php
$gioitinh = array(0 => 'Nam', 1 => 'Nữ');
$phankhoa = array("" => "", "MAT" => "Khoa học máy tính", "KDL" => "Khoa học vật liệu");
$err = array();

$err["hoten"] = "<label class='empty'></label><br>";
$err["gioitinh"] = "<label class='empty'></label><br>";
$err["phankhoa"] = "<label class='empty'></label><br>";
$err["ngaysinh"] = "<label class='empty'></label><br>";

if (isset($_POST['submit'])) {
    session_start();
    $_SESSION['hoten'] = "";
    $_SESSION['gioitinh'] = "";
    $_SESSION['phankhoa'] = "";
    $_SESSION['ngaysinh'] = "";

    $check_valid_date = "/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/";

    if (empty($_POST['hoten']))
        $err['hoten'] = "<label class='error'>Hãy nhập tên.</label><br>";
        
    if (empty($_POST['gioitinh']))
        $err['gioitinh'] = "<label class='error'>Hãy chọn giới tính.</label><br>";

    if (empty($_POST['phankhoa']))
        $err['phankhoa'] = "<label class='error'>Hãy chọn phân khoa. <br /></label>";


    $date = $_POST['ngaysinh'];

    function isValid($date, $format = 'd/m/Y'){
        $dt = DateTime::createFromFormat($format, $date);
        return $dt && $dt->format($format) === $date;
    }

    if (empty($_POST['ngaysinh']))
        $err['ngaysinh'] = "<label class='error'>Hãy chọn ngày sinh. <br /></label>";

    else
        if (isValid($date) == false)
            $err['ngaysinh'] = "<label class='error'>Hãy chọn đúng định dạng ngày sinh (d/m/Y).</label><br>";
}

echo
"<head>
    <meta charset='UTF-8'>
    <link rel='stylesheet' href='sinhvien.css'>
</head>

<body>
    <fieldset>
        <form method='post' action='sinhvien.php'>";
        echo $err["hoten"];
        echo $err["gioitinh"];
        echo $err["phankhoa"];
        echo $err["ngaysinh"];
echo "
            <table>

                <tr>
                    <td class='td'><label>Họ và tên <span>*</span> </label></td>
                    <td><input type='text' id='hoten' class='box' name='hoten' value='";
                    echo isset($_POST['hoten']) ? $_POST['hoten'] : '';
                    echo "'></td>
                </tr>

                <tr>
                    <td class='td'><label>Giới tính <span>*</span>  </label></td>
                    <td>";
                        for ($i = 0; $i < count($gioitinh); $i++) {
                            echo
                                "<input type='radio' name='gioitinh' class='box' value='" . $gioitinh[$i] . "'";
                            echo (isset($_POST['gioitinh']) && $_POST['gioitinh'] == $gioitinh[$i]) ? " checked " : "";
                            echo "/>" . $gioitinh[$i];
                        }
                        echo
                    "</td>
                </tr>
                
                <tr>
                    <td class='td'><label>Phân khoa <span>*</span> </label></td>
                    <td><select class='box' name='phankhoa'>";
                        foreach ($phankhoa as $key => $value) {
                            echo "<option";
                            echo (isset($_POST['phankhoa']) && $_POST['phankhoa'] == $key) ? " selected " : "";
                            echo " value='" . $key . "'>" . $value . "</option>";
                        }
                        echo "
                        </select></td>
                </tr>

                <tr>
                    <td class='td'><label>Ngày sinh <span>*</span> </label></td>
                    <td><input type='text' id='ngaysinh' class='box' name='ngaysinh'  value='";
                    echo isset($_POST['ngaysinh']) ? $_POST['ngaysinh'] : '';
                    echo "'></td>
                </tr>

                <tr>
                    <td class='td'><label>Địa chỉ</label></td>
                    <td><input type='text' id='diachi' class='box' name='diachi' value='";
                    echo "'></td>
                </tr>

            </table>

            <button name='submit' type='submit'>Đăng ký</button>
        </form>
    </fieldset>

</body>";
